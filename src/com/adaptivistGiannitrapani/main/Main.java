package com.adaptivistGiannitrapani.main;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.stream.Collectors;

/*
 * A::B refers to method B in class A. method refrence
 * 
 * HashMap.entrySet() method in Java is used to create a set out of the 
 * elements contained in the hash map. It basically returns a set view of 
 * the hash map or we can create a new set and store the map elements into them.
 * 
 * Set Interface. A Set is a Collection that cannot contain duplicate elements
 * 
 * Map.Entry interface in Java provides certain methods to access the entry in the Map
 * 
 * 
 * lambda take anonymus expressions as parameters
 * 
 * The method stream() is available on all collections in JDK 8 and it wraps the collection into an instance of Stream
 * A Stream is much like an iterator on a collection of objects and provides some nice fluent functions
 * 
 * 
 * 
 * */

public class Main {
	private static Map<String, Integer> createMapOrderByKey(Map<String, Integer> counts) {
		Map<String, Integer> countsKey = 
				counts.entrySet().stream()
				.sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
	                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		return countsKey;
	}
	
	private static Map<String, Integer> createMapOrderByValue(Map<String, Integer> counts) {
		Map<String, Integer> countsVal = 
				counts.entrySet().stream()
				.sorted(Map.Entry.comparingByValue())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));
		
		return countsVal;
	}
	
	private static void saveFiles(List<String> list, String saveFileK, String saveFileV) throws IOException {
		
		Map<String, Integer> counts = 
        		list.parallelStream()
        		.collect(Collectors.toConcurrentMap(w -> w, w -> 1, Integer::sum));
		
		Files.write(Path.of(saveFileK), () -> createMapOrderByKey(counts).entrySet().stream()
				.<CharSequence>map(e -> "word: '"+e.getKey()+ "', appearances: "+e.getValue())
				.iterator());
		Files.write(Path.of(saveFileV), () -> createMapOrderByValue(counts).entrySet().stream()
			.<CharSequence>map(e -> "word: '"+e.getKey()+ "', appearances: "+e.getValue())
			.iterator());
	}
	
	public static void main(String[] args) throws IOException {
		
		//asks the file to be analyzed 
		System.out.println("Please enter the file (.txt) path: ");
		Scanner filePath = new Scanner(System.in);
		String newFile= filePath.nextLine();
		
		try {
		//reads and splits the text file
		String content = Files.readString(Paths.get(newFile));
		List<String> list = Arrays.asList(content.split("(?=[,.])|\\s+"));
		
		//asks the path to save the files
		System.out.println("where do you want to save the file? please enter path: ");
		String saveFile = filePath.nextLine();
		String pathSlash = "\\";
		String saveFileK = saveFile+pathSlash+"counted_by_key";
		String saveFileV = saveFile+pathSlash+"counted_by_value";
		
		if(saveFile.endsWith("\\")) {
			saveFileK = saveFile+"counted_by_key";
			saveFileV = saveFile+"counted_by_value";
		}
		filePath.close();
		
		//create and save files
		saveFiles(list,saveFileK,saveFileV);
		
		//success
		System.out.println("files saved succesfully; have a look at them on: "+ saveFileK+" and "+ saveFileV);
		
		}catch (NoSuchFileException | InvalidPathException | AccessDeniedException | NoSuchElementException e
				) {
			//error
			System.out.println("It seems like you typed a wrong file path. please, try again.");
			main(args); 
		}
	
			
		
	}
}
